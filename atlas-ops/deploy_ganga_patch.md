# Deploy Ganga patch on development machine
Patch to Ganga is provided as a pull request in GitHub repository. Deploying the patch means that Ganga installation needs to be modified and the patch applied to proper file(s). 

The deployment of a Ganga patch can be performed by anyone on the development machines (e.g. `hammercloud-ai-33`), and only by the core team on the production machines. 

The patch deployment consists of the following steps: 

1. Receive Ganga pull URL from Ganga developer, e.g. [https://github.com/ganga-devs/ganga/pull/649](https://github.com/ganga-devs/ganga/pull/649). 
2. The patch URL can be assembled by adding `.patch` to the pull URL, e.g. [https://github.com/ganga-devs/ganga/pull/649.patch](https://github.com/ganga-devs/ganga/pull/649.patch). This is the patch you will apply on the Ganga installation. 
3. Learn location of Ganga installation that is used by your template. E.g. template [795](https://hammercloud.cern.ch/hc/admin/atlas/template/795/) uses **Gangabin** `/install/HEAD/bin/ganga`.
4. Locate directory with your Ganga installation. **Gangabin** `/install/HEAD/bin/ganga`
 corresponds to installation directory `${GANGA_DIR}/install/HEAD`, where where `GANGA_DIR=/data/hc/external/ganga `. However, the `HEAD` is a symlink: 

  ```
[root@hammercloud-ai-33 install]# ls -l /data/hc/external/ganga/install
total 36
lrwxrwxrwx. 1 root    root   68 Jun 13 16:36 HEAD -> /data/hc/external/ganga/install/6.1.2-hotfix1.patch2016-05-12.HC-310
drwxrwxr-x. 7 root    root 4096 Nov  4  2015 6.1.13
...
drwxr-xr-x. 8 root    root 4096 Jan 20 09:34 DEV
drwxr-xr-x. 7 root    root 4096 Jan 19 17:59 DEV_BACKUP
drwxr-xr-x. 8 root    root 4096 Jan 19 18:02 TEST
```
Therefore, the real Ganga installation directory is `/data/hc/external/ganga/install/6.1.2-hotfix1.patch2016-05-12.HC-310`. 

5. Create backup of you Ganga installation directory. 
 
 ```
 # cd /data/$USER 
 
 ### create tarball with the Ganga installation directory
 # tar cf ganga.backup.HEAD.`date +%F.%H%M%S`.tar /data/hc/external/ganga/install/6.1.2-hotfix1.patch2016-05-12.HC-310
 ```
6. Download the patch.

  ```
  ### go to the Ganga installation directory
  # cd /data/hc/external/ganga/install/HEAD

  ### download the patch
  ### using wget because it can follow HTTP redirections without additional parameters
  # wget https://github.com/ganga-devs/ganga/pull/649.patch -O /tmp/649.patch

  # ls -l /tmp/649.patch
  -rw-r--r--. 1 root root 2755 Jul  2 11:38 /tmp/649.patch  
  ```
7. Apply the patch.
 
 ```
 # cd /data/hc/external/ganga/install/HEAD

 ### apply the patch
 # patch -p1 < /tmp/649.patch

 patching file python/GangaPanda/Lib/ProdTrans/ProdTransPandaRTHandler.py
 Hunk #1 succeeded at 215 (offset -9 lines).
patching file python/GangaPanda/Lib/ProdTrans/ProdTransPandaRTHandler.py
 Hunk #1 succeeded at 218 (offset -9 lines).

  ```

8. That's it! Enjoy!



Deployment to production machines differs in the following: 

  * The full directories with patch applied are distributed.
  * Symlink of `HEAD` is pointed to the new directory. This is done in Puppet, so any manual change of symlink is overwritten by Puppet within 30 minutes.
