# Commissioning/Decommissioning of AFTs and PFTs

### Template requirements
The jobs for the HammerCloud blacklisting/whitelisting should be fairly fast and produce reasonably small output. Make sure that the template for a new AFT/PFT creates jobs that take at most around 10 minutes to process and generate only a couple MB of output.

### Checklist after new template is created/tested
* Commit the template code and additional files to the master branch of [hammercloud-atlas-inputfiles](https://gitlab.cern.ch/hammercloud/hammercloud-atlas-inputfiles)
* Build a new RPM for [hammercloud-atlas-inputfiles](https://gitlab.cern.ch/hammercloud/hammercloud-atlas-inputfiles) according to the instructions [here](https://twiki.cern.ch/twiki/bin/viewauth/IT/HammerCloudAgile#Release_a_new_version_of_the_pac)
* Deploy the new RPM everywhere (all ATLAS submit nodes and hc80), i.e. request the deployment via ticket to hammercloud-tickets@cern.ch
* Make sure the new template description in [HC admin](https://hammercloud.cern.ch/hc/admin/atlas/template) contains exactly `PFT` or `AFT`, i.e. remove dots from `P.F.T.` if you previously used this as a template description -- this will ensure that the list of PanDA Resources is automatically updated for this template by AGIS collector
* In [HC admin](https://hammercloud.cern.ch/hc/admin/atlas/template), for the new template, set
    * **type: functional**
    * **active: true**
    * **period: 0**
    * **lifetime: 1**
    * **golden: true**

Now that both old+new templates are golden, they will both be taken into account for blacklisting.

* Wait several hours (e.g. min 1h, blacklisting runs every 30 minutes and takes into account past 3 hours) for jobs of the new template to start & finish.

and then

* In [HC admin](https://hammercloud.cern.ch/hc/admin/atlas/template), for the old template, set
    * **active: true**
    * **golden: false**

This removes the old template from the black/whitelisting process but leaves it still running.

* After a grace period, when you are sure that the new template runs without problems (hint: the blacklisting report that is send to hammercloud-notifications-atlas indicates for which sites there are too few test jobs), deactivate the old template (**active: false**).


### Possible issues
* Not enough jobs of the new template for a PanDA Resource --> will not be black/whitelisted until there are enough jobs to follow the policies. You can see that from table(s) on e.g. [here](http://hammercloud.cern.ch/hc/app/atlas/siteoverview/?site=AGLT2&startTime=2018-09-03&endTime=2018-09-11&templateType=isGolden)
    * If you notice that the job throughput of the new template is too low, check the priority of the jobs. It should be 4000 for AFTs and 10000 for PFTs.
* The policies seem to be built around 3 templates, with temporarily having 4 might introduce issues ==> disable the old template (**golden: false**) and enable the new template (**golden: true**) quickly
* The template description string **must** start with "AFT " or "PFT " for AFTs and PFTs, respectively. Otherwise HC won't be able to properly assign the template to one or the other.
* The processingtype **must** be `gangarobot` for AFTs and `gangarobot-pft` for PFTs, otherwise the automatic priority assigning of HammerCloud doesn't work properly.