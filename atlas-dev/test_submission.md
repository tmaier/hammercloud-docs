# Test submission

**Test submission:** first bunch of jobs from configuration files are submitted to PanDA.

## Test submission in ``celery_prod``
* Main script: [apps/atlas/python/scripts/submittest_celery_prod.py](https://gitlab.cern.ch/hammercloud/hammercloud-atlas/blob/master/apps/atlas/python/scripts/submittest_celery_prod.py)

  * main function: ``SubmittestCeleryProd.run()``
* Main wrapper: [/data/hc/scripts/submit/test-run.sh](https://gitlab.cern.ch/hammercloud/hammercloud-core/blob/master/hc_core/scripts/submit/test-run.sh#L56)


## Test submission in ``celery_prod_jedi``
* Main script: [apps/atlas/python/scripts/submittest_celery_jedi.py](https://gitlab.cern.ch/hammercloud/hammercloud-atlas/blob/master/apps/atlas/python/scripts/submittest_celery_jedi.py)

  * main function: ``SubmittestCeleryJedi.run()``
* Main wrapper: [/data/hc/scripts/submit/test-run.sh](https://gitlab.cern.ch/hammercloud/hammercloud-core/blob/master/hc_core/scripts/submit/test-run.sh#L74)
